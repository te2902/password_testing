const { checkLength, checkAlphabet, checkSymbol, checkDigit, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1010101010101010101010101')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('10101010101010101010101010')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('a')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has number in password', () => {
    expect(checkDigit('1234')).toBe(true)
  })
  test('should has not number in password', () => {
    expect(checkDigit('asdf')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('1!11')).toBe(true)
  })

  test('should has not symbol ! in password to be false', () => {
    expect(checkSymbol('1111')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password 12345678!password to be true', () => {
    expect(checkPassword('12345678!password')).toBe(true)
  })

  test('should password 12345 to be false', () => {
    expect(checkPassword('12345')).toBe(false)
  })
})
